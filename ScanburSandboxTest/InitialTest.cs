﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using ScanburSandbox.Models;

namespace ScanburSandboxTest
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class InitialTest
    {

        [Fact]
        public void TestDataAccess()
        {
            Guid? mId=null;
            Guid? unitId = null;
            ApplicationDbContext.IsTest = true;
            using (var db = new ApplicationDbContext())
            {

                int bef = db.ScanClimeUnits.Count();
                ScanClimeUnit sc = new ScanClimeUnit();
                sc.Initialize();
                sc.Title = "My Title";
                sc.UpdatedAt = DateTime.Now;
                db.ScanClimeUnits.Add(sc);
                db.SaveChanges();
                int after = db.ScanClimeUnits.Count();
                Assert.True(after == bef + 1);

                bef = db.BackendPosts.Count();
                BackendPost post = new BackendPost();
                post.Initialize();
                post.Json = JSON;
                post.ScanClimeUnitId = sc.Id;
                db.BackendPosts.Add(post);
                db.SaveChanges();
                after = db.BackendPosts.Count();
                Assert.True(after == bef + 1);

                bef = db.Measurements.Count();
                Measurement m = new Measurement();
                m.Initialize();
                db.Measurements.Add(m);
                db.SaveChanges();
                mId = m.Id;
                after = db.Measurements.Count();
                Assert.True(after == bef + 1);

                sc.CurrentMeasurementId = m.Id;
                sc.CurrentMeasurement = m;
                sc.Title += " XXX";
                db.ScanClimeUnits.Update(sc);
                db.SaveChanges();
                unitId = sc.Id;
                sc = null;
            }
            using (var db = new ApplicationDbContext())
            {
                ScanClimeUnit sc = db.ScanClimeUnits.Single(s => s.Id == unitId);
                Assert.NotNull(sc.CurrentMeasurementId);
                Assert.Equal((Guid)sc.CurrentMeasurementId, mId);
                //Verify lazy-loading
                //TODO: Fix lazy loading - it is not working
               // Assert.NotNull(sc.CurrentMeasurement);
                //Assert.Equal(sc.CurrentMeasurement.Id, mId);
            }
        }

        [Fact]
        public void TestControllerCreate()
        {
            var db = GetDb();
            int bef = db.ScanClimeUnits.Count();
            ScanClimeUnit sc = new ScanClimeUnit();
            sc.Initialize();
            sc.Title = "My Title";
            sc.UpdatedAt = DateTime.Now;
            new ScanburSandbox.Controllers.ScanClimeApiController().Create(sc);

            int after = db.ScanClimeUnits.Count();
            Assert.True(after == bef + 1);

        }

        [Fact]
        public void TestControllerPostJson()
        {
            Guid unitId;
            Guid mId;
            using (var db = GetDb())
            {
                var unit = db.ScanClimeUnits.First();
                unitId = unit.Id;
                unit.CurrentMeasurementId = null;
                db.SaveChanges();
            }
            using (var db = GetDb())
            {

                var unit = db.ScanClimeUnits.Single(s => s.Id == unitId);
                int bef = db.BackendPosts.Count();
                int bef2 = db.Measurements.Count();
                var output = new ScanburSandbox.Controllers.ScanClimeApiController().PostJsonUtil(unit.IdStr, JSON);
                mId = output.Measurement.Id;
                int after = db.BackendPosts.Count();
                int after2 = db.Measurements.Count();
                Assert.True(after == bef + 1);
                Assert.True(after2 == bef2 + 1);

            }

            using (var db = GetDb())
            {
                var unit = db.ScanClimeUnits.Single(s => s.Id == unitId);
                Assert.NotNull(unit.CurrentMeasurementId);
                Assert.Equal(unit.CurrentMeasurementId, mId);
            }

        }

        [Fact]
        public void TestControllerPostJsonInvalid()
        {
            Guid unitId;
            using (var db = GetDb())
            {
                var unit = db.ScanClimeUnits.First();
                unitId = unit.Id;
                unit.CurrentMeasurementId = null;
                db.SaveChanges();
            }
            using (var db = GetDb())
            {

                var unit = db.ScanClimeUnits.Single(s => s.Id == unitId);
                int bef = db.BackendPosts.Count();
                int bef2 = db.Measurements.Count();
                var output = new ScanburSandbox.Controllers.ScanClimeApiController().PostJson(unit.IdStr, JSONinvalid);

                int after = db.BackendPosts.Count();
                int after2 = db.Measurements.Count();
                Assert.True(after == bef + 1);//One backendPost was produced. 
                Assert.True(after2 == bef2); //No measurements were produced

            }

        }
        [Fact]
        public void TestControllerPostJsonUnknownUnit()
        {
            
            using (var db = GetDb())
            {
                
                int bef = db.BackendPosts.Count();
                int bef2 = db.Measurements.Count();
                var output = new ScanburSandbox.Controllers.ScanClimeApiController().PostJson(Guid.NewGuid().ToString(), JSONinvalid);

                int after = db.BackendPosts.Count();
                int after2 = db.Measurements.Count();
                Assert.True(after == bef + 1);//One backendPost was produced. 
                Assert.True(after2 == bef2); //No measurements were produced

            }

        }

        private static ApplicationDbContext GetDb()
        {

            ApplicationDbContext.IsTest = true;
            return new ApplicationDbContext();
        }

        private String JSON = @"
{
 ""backendmsg"": ""Infomsg"",
 ""measurements"": [
 {
 ""Actuator1"": 2230.0830078125,
 ""Actuator10"": 0,
 ""Actuator11"": 0,
 ""Actuator12"": 1,
 ""Actuator2"": 0,
 ""Actuator3"": 0,
 ""Actuator4"": 0,
 ""Actuator5"": 1.9046890735626221,
 ""Actuator6"": 1.5319864749908447,
 ""Actuator7"": 0,
 ""Actuator8"": 0,
 ""Actuator9"": 0,
 ""AirChange"": 75,
 ""AirChangeActual"": 75.210660521609626,
 ""AirFlowDiff"": 20,
 ""AirvolumeDiff"": 20.066678138149825,
 ""ChillWaterValvePCT"": 0,
 ""HeaterTempSP"": 126.06194428943452,
 ""MixBedFilter"": 14.536907828564379,
 ""PressureDiff"": 1,
 ""RelativeHumidityRoomFixed"": 55,
 ""Sensor1"": 250,
 ""Sensor10"": 0.45115995115995117,
 ""Sensor11"": 1,
 ""Sensor12"": 21.842594999999999,
 ""Sensor13"": 21.48910600002878,
 ""Sensor14"": 684,
 ""Sensor15"": 564,
 ""Sensor2"": 33.654411556212487,
 ""Sensor3"": 21.363319531915462,
 ""Sensor30"": 1.2278206770665601,
 ""Sensor31"": 0.97927543745646317,
 ""Sensor32"": 6.3214862758907042,
 ""Sensor33"": 6.3847435386166751,
 ""Sensor34"": 10.093114700547368,
 ""Sensor35"": 34.74732516098365,
 ""Sensor36"": 27.713494880017908,
 ""Sensor37"": 1.2243816254416959,
 ""Sensor38"": 0.97950530035335681,
 ""Sensor4"": 34.447418412388927,
 ""Sensor40"": 34.792123644931237,
 ""Sensor5"": 20.822352132089865,
 ""Sensor6"": 28.717948717948719,
 ""Sensor7"": 0.75936375282901536,
 ""Sensor8"": 0.89011974358524393,
 ""Sensor9"": 2.7252747252747254,
 ""Timestamp"": 1455048461451,
 ""steppermotorPCT"": 5.3097214471726186
 }
 ]
}
";

        private String JSONinvalid = @"

 backendmsg"": ""Infomsg"",
 ""measurements"": [
 {
 ""Actuator1"": 2230.0830078125,
 ""Actuator10"": 0,
 ""Actuator11"": 0,
 ""Actuator12"": 1,
 ""Actuator2"": 0,
 ""Actuator3"": 0,
 ""Actuator4"": 0,
 ""Actuator5"": 1.9046890735626221,
 ""Actuator6"": 1.5319864749908447,
 ""Actuator7"": 0,
 ""Actuator8"": 0,
 ""Actuator9"": 0,
 ""AirChange"": 75,
 ""AirChangeActual"": 75.210660521609626,
 ""AirFlowDiff"": 20,
 ""AirvolumeDiff"": 20.066678138149825,
 ""ChillWaterValvePCT"": 0,
 ""HeaterTempSP"": 126.06194428943452,
 ""MixBedFilter"": 14.536907828564379,
 ""PressureDiff"": 1,
 ""RelativeHumidityRoomFixed"": 55,
 ""Sensor1"": 250,
 ""Sensor10"": 0.45115995115995117,
 ""Sensor11"": 1,
 ""Sensor12"": 21.842594999999999,
 ""Sensor13"": 21.48910600002878,
 ""Sensor14"": 684,
 ""Sensor15"": 564,
 ""Sensor2"": 33.654411556212487,
 ""Sensor3"": 21.363319531915462,
 ""Sensor30"": 1.2278206770665601,
 ""Sensor31"": 0.97927543745646317,
 ""Sensor32"": 6.3214862758907042,
 ""Sensor33"": 6.3847435386166751,
 ""Sensor34"": 10.093114700547368,
 ""Sensor35"": 34.74732516098365,
 ""Sensor36"": 27.713494880017908,
 ""Sensor37"": 1.2243816254416959,
 ""Sensor38"": 0.97950530035335681,
 ""Sensor4"": 34.447418412388927,
 ""Sensor40"": 34.792123644931237,
 ""Sensor5"": 20.822352132089865,
 ""Sensor6"": 28.717948717948719,
 ""Sensor7"": 0.75936375282901536,
 ""Sensor8"": 0.89011974358524393,
 ""Sensor9"": 2.7252747252747254,
 ""Timestamp"": 1455048461451,
 ""steppermotorPCT"": 5.3097214471726186
 }
 ]
}
";

        private String JSONWithId = @"
{
""unitId"":""{0}"",
""data"":
{
 ""backendmsg"": ""Infomsg"",
 ""measurements"": [
 {
 ""Actuator1"": 2230.0830078125,
 ""Actuator10"": 0,
 ""Actuator11"": 0,
 ""Actuator12"": 1,
 ""Actuator2"": 0,
 ""Actuator3"": 0,
 ""Actuator4"": 0,
 ""Actuator5"": 1.9046890735626221,
 ""Actuator6"": 1.5319864749908447,
 ""Actuator7"": 0,
 ""Actuator8"": 0,
 ""Actuator9"": 0,
 ""AirChange"": 75,
 ""AirChangeActual"": 75.210660521609626,
 ""AirFlowDiff"": 20,
 ""AirvolumeDiff"": 20.066678138149825,
 ""ChillWaterValvePCT"": 0,
 ""HeaterTempSP"": 126.06194428943452,
 ""MixBedFilter"": 14.536907828564379,
 ""PressureDiff"": 1,
 ""RelativeHumidityRoomFixed"": 55,
 ""Sensor1"": 250,
 ""Sensor10"": 0.45115995115995117,
 ""Sensor11"": 1,
 ""Sensor12"": 21.842594999999999,
 ""Sensor13"": 21.48910600002878,
 ""Sensor14"": 684,
 ""Sensor15"": 564,
 ""Sensor2"": 33.654411556212487,
 ""Sensor3"": 21.363319531915462,
 ""Sensor30"": 1.2278206770665601,
 ""Sensor31"": 0.97927543745646317,
 ""Sensor32"": 6.3214862758907042,
 ""Sensor33"": 6.3847435386166751,
 ""Sensor34"": 10.093114700547368,
 ""Sensor35"": 34.74732516098365,
 ""Sensor36"": 27.713494880017908,
 ""Sensor37"": 1.2243816254416959,
 ""Sensor38"": 0.97950530035335681,
 ""Sensor4"": 34.447418412388927,
 ""Sensor40"": 34.792123644931237,
 ""Sensor5"": 20.822352132089865,
 ""Sensor6"": 28.717948717948719,
 ""Sensor7"": 0.75936375282901536,
 ""Sensor8"": 0.89011974358524393,
 ""Sensor9"": 2.7252747252747254,
 ""Timestamp"": 1455048461451,
 ""steppermotorPCT"": 5.3097214471726186
 }
 ]
}
}
";
        

    }
}
