﻿using ScanburSandbox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScanburSandbox.ViewModels.ScanClime
{
    public class ScanClimeIndexVM
    {
        public ScanClimeUnit Scunit {get; set; }
        public Measurement Measurement { get; set; }
        public List<ScanClimeUnit> AllUnitIds { get; set; }
    }
}
