using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace ScanburSandbox.Migrations
{
    public partial class modelv3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.AlterColumn<double>(
                name: "SteppermotorPCT",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AlterColumn<double>(
                name: "RelativeHumidityRoomFixed",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AlterColumn<double>(
                name: "PressureDiff",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AlterColumn<double>(
                name: "MixBedFilter",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AlterColumn<double>(
                name: "HeaterTempSP",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AlterColumn<double>(
                name: "ChillWaterValvePCT",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AlterColumn<double>(
                name: "AirvolumeDiff",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AlterColumn<double>(
                name: "AirFlowDiff",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AlterColumn<double>(
                name: "AirChangeActual",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AddColumn<double>(
                name: "CageHumidity",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AddColumn<double>(
                name: "InletTemperature",
                table: "Measurement",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropColumn(name: "CageHumidity", table: "Measurement");
            migrationBuilder.DropColumn(name: "InletTemperature", table: "Measurement");
            migrationBuilder.AlterColumn<double>(
                name: "SteppermotorPCT",
                table: "Measurement",
                nullable: false);
            migrationBuilder.AlterColumn<double>(
                name: "RelativeHumidityRoomFixed",
                table: "Measurement",
                nullable: false);
            migrationBuilder.AlterColumn<double>(
                name: "PressureDiff",
                table: "Measurement",
                nullable: false);
            migrationBuilder.AlterColumn<double>(
                name: "MixBedFilter",
                table: "Measurement",
                nullable: false);
            migrationBuilder.AlterColumn<double>(
                name: "HeaterTempSP",
                table: "Measurement",
                nullable: false);
            migrationBuilder.AlterColumn<double>(
                name: "ChillWaterValvePCT",
                table: "Measurement",
                nullable: false);
            migrationBuilder.AlterColumn<double>(
                name: "AirvolumeDiff",
                table: "Measurement",
                nullable: false);
            migrationBuilder.AlterColumn<double>(
                name: "AirFlowDiff",
                table: "Measurement",
                nullable: false);
            migrationBuilder.AlterColumn<double>(
                name: "AirChangeActual",
                table: "Measurement",
                nullable: false);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
