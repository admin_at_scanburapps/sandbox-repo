﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ScanburSandbox.Models;
using ScanburSandbox.ViewModels.ScanClime;
using ScanburSandbox.Facades;

namespace ScanburSandbox.Controllers
{
    
    public class ScanClimeController : Controller
    {

        private Facade fac = Facade.Instance;

        public IActionResult Index(Guid? id)
        {
            using(var db = fac.NewDbContext)
            {
                ScanClimeUnit scu;
                if (id == null )
                {
                    scu = fac.ScanClimeUnitFac.GetFirst(db);
                    this.RedirectToAction("Index", new { id = scu.Id });
                } else
                {
                    scu = fac.ScanClimeUnitFac.GetOrFail(db, id); 
                }
                var ms = Facade.Instance.MeasurementFc.Get(db,scu.CurrentMeasurementId); 
                return View(new ScanClimeIndexVM() { Scunit = scu, Measurement = ms, AllUnitIds = fac.ScanClimeUnitFac.GetAll(db) }); 
            }
        }

        public IActionResult List()
        {
            using(var db = Facade.Instance.NewDbContext)
            {
                var scunits = db.ScanClimeUnits.ToList();
                return View(new ScanClimeListVM() {
                    Scunits = scunits
                });
            }
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();

        }


        private static ApplicationDbContext GetDb()
        {
            return new ApplicationDbContext();
        }

        


    }
}
