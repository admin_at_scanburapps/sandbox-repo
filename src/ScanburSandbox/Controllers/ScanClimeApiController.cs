﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ScanburSandbox.Models;
using Newtonsoft.Json.Linq;

namespace ScanburSandbox.Controllers
{
    [Route("/api/ScanClime")]
    public class ScanClimeApiController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();

        }


        private static ApplicationDbContext GetDb()
        {
            return new ApplicationDbContext();
        }


        [HttpGet("GetAll")]
        public IEnumerable<ScanClimeUnit> GetAll()
        {
            using (var db = GetDb())
            {
                return db.ScanClimeUnits.ToList();
            }
        }

        [HttpGet("Get/{id}")]
        public IActionResult Get(string id)
        {
            using (var db = GetDb())
            {
                var x = db.ScanClimeUnits.Where(s => s.IdStr == id);
                if (x==null || x.Count()==0)
                {
                    throw new Exception("Could not find unit with ID: "+id);
                }
                return new ObjectResult(x.First());
            }
        }


        [HttpPost("Create")]
        public IActionResult Create([FromBody] ScanClimeUnit unit)
        {
            if (unit == null)
            {
                throw new InvalidProgramException("Unit data cannot be null");
            }
            using (var db = GetDb())
            {
                unit.Initialize();
                db.ScanClimeUnits.Add(unit);
                db.SaveChanges();
                return new ObjectResult(unit);
            }
        }

        [HttpPut("Update")]
        public IActionResult Update([FromBody] ScanClimeUnit unit)
        {

            using (var db = GetDb())
            {
                if (unit == null)
                {
                    throw new Exception("Posted unit cannot be null.");
                }
                var unit2 = db.ScanClimeUnits.Where(s => s.IdStr == unit.IdStr).ToList().First();
                if (unit2 == null)
                {
                    throw new Exception("No unit found with id: " + unit.Id);
                }
                unit2.Title = unit.Title;
                unit2.CreatedAt = unit.CreatedAt;
                unit2.UpdatedAt = DateTime.Now;
                db.ScanClimeUnits.Update(unit2);
                return new ObjectResult(unit2);
            }
        }

        [HttpPost("PostJson1")]
        public IActionResult PostJson1([FromBody] dynamic data)
        {
            using (var db = GetDb())
            {
                BackendPost post = new BackendPost();
                post.Initialize();
                post.Processed = false;
                post.Json = data.ToString();
                db.BackendPosts.Add(post);
                db.SaveChangesAsync();
                return new ObjectResult("OK");
            }
        }

        [HttpPost("PostJson/{unitId}")]
        public IActionResult PostJson(String unitId, [FromBody]Newtonsoft.Json.Linq.JToken jsonbody)
        {
            var result = PostJsonUtil(unitId, jsonbody);
            return new ObjectResult(new { MeasurementId = ((result.Measurement!=null ? result.Measurement.IdStr:"")) , BackendPostId = result.BackendPost.IdStr});
        }

        /// <summary>
        /// This is only exposed for testing purposes
        /// TODO: Move to a seperate class or make sure it cannot be called through the REST-interface
        /// </summary>
        /// <param name="unitId"></param>
        /// <param name="jsonbody"></param>
        /// <returns></returns>
        public PostJsonResult PostJsonUtil(string unitId, JToken jsonbody)
        {
            using (var db = GetDb())
            {
                BackendPost post = BackendPost.Create();
                post.Initialize();
                post.Processed = false;
                post.Json = jsonbody.ToString();
                post.ScanClimeUnitId = new Guid(unitId);
                db.BackendPosts.Add(post);
                db.SaveChanges();
                var ms = ParseJson(post.Id);
                return new PostJsonResult()
                {
                    BackendPost = post,
                    Measurement = ms
                };
            }
        }

        private Measurement ParseJson(Guid backendPostId)
        {
            using (var db = GetDb())
            {
                var post = db.BackendPosts.Single(p => p.Id == backendPostId);
                try {
                    dynamic jsonObj, data;
                    try
                    {
                        jsonObj = JObject.Parse(post.Json);
                    } catch(Exception e)
                    {
                        throw new Exception("JSON invalid for BackendPost.Id: " + backendPostId, e);
                    }
                    try
                    {
                        data = jsonObj.measurements[0];
                    } catch(Exception e)
                    {
                        throw new Exception("JSON does not contain measurement field for BackendPost.Id: " + backendPostId, e);
                    }
                    Measurement ms = Measurement.Create();
                    ms.UnitId = post.ScanClimeUnitId;
                    ms.AirChangeActual = ParseDouble(data.AirChangeActual);
                    ms.RelativeHumidityRoomFixed = ParseDouble(data.RelativeHumidityRoomFixed);
                    ms.AirFlowDiff = ParseDouble(data.AirFlowDiff);
                    ms.AirvolumeDiff = ParseDouble(data.AirvolumeDiff);
                    ms.ChillWaterValvePCT = ParseDouble(data.ChillWaterValvePCT);
                    ms.HeaterTempSP = ParseDouble(data.HeaterTempSP);
                    ms.MixBedFilter = ParseDouble(data.MixBedFilter);
                    ms.PressureDiff = ParseDouble(data.PressureDiff);
                    ms.Timestamp = data.Timestamp;
                    ms.SteppermotorPCT = ParseDouble(data.steppermotorPCT);
                    ms.InletTemperature = ParseDouble(data.Sensor5);
                    ms.CageHumidity = ParseDouble(data.Sensor40);

                    ///For now we will just use the present time. Can be improved later. 
                    ms.MeasurementTime = DateTime.Now;
                    db.Measurements.Add(ms);
                    //Update the current measurement
                    var sc = db.ScanClimeUnits.Single(s => s.Id == ms.UnitId);
                    if (sc!= null)
                    {
                        sc.CurrentMeasurementId = ms.Id;
                        sc.CurrentMeasurement = ms;
                        sc.OnSave();
                        post.Status = BackendPostStatusEnum.Ok;
                        post.OnSave();

                    } else
                    {
                        throw new Exception("No ScanClimeUnit found for Id:" + ms.UnitId);
                    }

                    db.SaveChanges();
                    return ms;

                }
                catch (Exception e)
                {
                    post.Status = BackendPostStatusEnum.NotOk;
                    var msg = (e.Message + " " + e.StackTrace);
                    post.ErrorMessage = Truncate(msg,4000);
                    db.SaveChanges();
                    return null;
                } finally
                {
                    post.Processed = true;
                }
                
            }
        }

        private string Truncate(string msg, int max)
        {
            if (msg.Length < max) return msg;
            else return msg.Substring(0, max - 1);
        }

        private double ParseDouble(dynamic val)
        {
            
            double d;
            if (val == null) return 0;
            if (val.Value is double || val.Value is float || val.Value is int || val.Value is long) return (double)val;
            try
            {
                if (double.TryParse(val, out d)) return d;
                else throw new Exception("Value cannot be parsed as double: " + val);
            } catch(Exception e)
            {
                throw new Exception("Could not parse: " + val.Value, e);
            }
        }

        [HttpPost]
        public IActionResult PostJson3([FromBody] dynamic data)
        {
            using (var db = GetDb())
            {
                BackendPost post = new BackendPost();
                post.Initialize();
                post.Processed = false;

                post.Json = data.ToString();
                db.BackendPosts.Add(post);
                db.SaveChanges();
                return new ObjectResult("OK");
            }
        }


    }
}
