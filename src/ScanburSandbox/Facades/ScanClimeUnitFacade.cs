﻿using System;
using ScanburSandbox.Models;
using System.Linq;
using System.Collections.Generic;

namespace ScanburSandbox.Facades
{
    public class ScanClimeUnitFacade 
    {
        FacadeUtil<ScanClimeUnit> util = new FacadeUtil<ScanClimeUnit>();
        internal ScanClimeUnit GetFirst(ApplicationDbContext db)
        {
            return db.ScanClimeUnits.First();
        }

        /// <summary>
        /// Return a ScanClimeUnit by Id or null if a null Id is passed.
        /// If an invalid Id is passed an exception is thrown. 
        /// </summary>
        /// <param name="db"></param>
        /// <param name="scunitId"></param>
        /// <returns></returns>
        internal ScanClimeUnit Get(ApplicationDbContext db, Guid? scunitId)
        {
            if (scunitId == null) return null;
            return db.ScanClimeUnits.Single(s => s.Id == scunitId);
        }

        internal ScanClimeUnit GetOrFail(ApplicationDbContext db, Guid? scunitId)
        {
            return util.GetOrFail(this.Get, db, scunitId);
        }

        internal List<ScanClimeUnit> GetAll(ApplicationDbContext db)
        {
            return db.ScanClimeUnits.ToList();
        }
    }
}