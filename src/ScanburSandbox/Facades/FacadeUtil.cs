﻿using System;
using ScanburSandbox.Models;

namespace ScanburSandbox.Facades
{
    internal class FacadeUtil<T>
    {
        private Guid NULLGUID = new Guid("00000000-0000-0000-0000-000000000000");
        internal T GetOrFail(Func<ApplicationDbContext, Guid?, T> getter, ApplicationDbContext db, Guid? id)
        {
            if (id == null) throw new ArgumentNullException("The Id cannot be null when fetching an object of type " + typeof(T));
            if (id.ToString().Trim() == "") throw new ArgumentNullException("The Id cannot be an empty string or whitespace-only string when fetching an object of type " + typeof(T));
            if (id.Equals(NULLGUID)) throw new ArgumentNullException("The Id cannot be " + NULLGUID + " when fetching an object of type " + typeof(T));
            T result = getter(db, id);
            if (result == null) throw new InvalidOperationException("The id " + id + " does not match an object of type " + typeof(T));
            return result;
        }
    }
}