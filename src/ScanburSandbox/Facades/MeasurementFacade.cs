﻿using System;
using ScanburSandbox.Models;

using System.Linq;
namespace ScanburSandbox.Facades
{
    public class MeasurementFacade
    {
        /// <summary>
        /// Returns a Measurement by ID or null if mId is null.
        /// If an ID is provided and none is found, an exception is thrown.
        /// </summary>
        /// <param name="db"></param>
        /// <param name="mId"></param>
        /// <returns></returns>
        internal Measurement Get(ApplicationDbContext db, Guid? mId)
        {
            if (mId == null) return null;
            return db.Measurements.Single(m => m.Id == mId);
        }
    }
}