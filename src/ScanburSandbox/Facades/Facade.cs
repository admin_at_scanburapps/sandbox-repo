﻿using ScanburSandbox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScanburSandbox.Facades
{
    public class Facade
    {
        private Facade() { }
        public readonly static Facade Instance = new Facade();

        public readonly MeasurementFacade MeasurementFc = new MeasurementFacade();

        /// <summary>
        /// A common way to get a reference to the DbContext.
        /// </summary>
        /// <returns></returns>
        public ApplicationDbContext NewDbContext
        {
            get { return new ApplicationDbContext(); }
        }

        public readonly ScanClimeUnitFacade ScanClimeUnitFac = new ScanClimeUnitFacade();
    }
}
