﻿using ScanburSandbox.Models;

namespace ScanburSandbox.Controllers
{
    public class PostJsonResult
    {
        public BackendPost BackendPost { get; internal set; }
        public Measurement Measurement { get; internal set; }
    }
}