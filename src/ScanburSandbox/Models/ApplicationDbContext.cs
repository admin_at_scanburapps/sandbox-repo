﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;

namespace ScanburSandbox.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public static bool IsTest = false; 
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //This is a mess. Configuration of connection string must be injected properly for these cases:
            //-Local DEV environment
            //- Azure environments
            //- XUnit tests
            if (IsTest)
            {
                optionsBuilder.UseSqlServer("Server = localhost\\SQLEXPRESS; Database = d-sdmwebdb; Trusted_Connection = True; MultipleActiveResultSets = true");
            } else {
                //This does not work: TODO: Find out why: base.OnConfiguring(optionsBuilder);

                //And this should not be necessary. TODO: Find out why. 
                optionsBuilder.UseSqlServer(ScanburSandbox.Startup.StaticConfiguration["Data:DefaultConnection:ConnectionString"]);
                

            }

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);


            builder.Entity<ScanClimeUnit>()
               .HasOne(p => p.CurrentMeasurement);

        }
        public DbSet<ScanClimeUnit> ScanClimeUnits { get; set; }
        public DbSet<BackendPost> BackendPosts { get; set; }
        public DbSet<Measurement> Measurements { get; set; }
    }
}
