﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScanburSandbox.Models
{
    public class BackendPost : Entity
    {
        /// <summary>
        /// This is a reference to the ScanClime unit for which this measurement was created.
        /// It is by intention that there is no foreign key reference to the ScanClimeUnit entity 
        /// as we don't want the posted data to fail if the Unit does not exist.
        /// </summary>
        public Guid? ScanClimeUnitId { get; set; }

        [Required]
        [MaxLength(4000)]
        public string Json { get; set; }
        public bool Processed { get; set; } = false;

        [System.ComponentModel.DataAnnotations.EnumDataType(typeof(BackendPostStatusEnum))]
        public BackendPostStatusEnum Status { get; set; }

        [System.ComponentModel.DataAnnotations.DataType(DataType.MultilineText)]
        [MaxLength(4000)]
        public string ErrorMessage { get; set; }

        internal static BackendPost Create()
        {
            var post = new BackendPost();
            post.Initialize();
            return post;
        }
    }

    public enum BackendPostStatusEnum
    {
        Ok=1, NotOk=2
    }
}
