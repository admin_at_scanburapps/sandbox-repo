﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScanburSandbox.Models
{
    public class ScanClimeUnit : Entity
    {
        [MaxLength(100)]
        public string Title { get; set; }


        /// <summary>
        /// The most recent measurement for this ScanClime unit.
        /// </summary>
        public Measurement CurrentMeasurement { get; set; }
        public Guid? CurrentMeasurementId { get; set; }


        //public List<Measurement> Measurements { get; set; }
    }
}
