﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScanburSandbox.Models
{
    /// <summary>
    /// This class is included just to show how a best practice implementation of our Entity class can be made. 
    /// TODO: merge in the important part from this class into our Entity class. 
    /// </summary>
    public abstract class EntityBestPractice
    {
        public virtual long Id { get; protected set; }

        public override bool Equals(object obj)
        {
            var compareTo = obj as EntityBestPractice;

            if (ReferenceEquals(compareTo, null))
                return false;

            if (ReferenceEquals(this, compareTo))
                return true;

            if (GetRealType() != compareTo.GetRealType())
                return false;

            if (!IsTransient() && !compareTo.IsTransient() && Id == compareTo.Id)
                return true;

            return false;
        }

        public static bool operator ==(EntityBestPractice a, EntityBestPractice b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(EntityBestPractice a, EntityBestPractice b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return (GetRealType().ToString() + Id).GetHashCode();
        }

        public virtual bool IsTransient()
        {
            return Id == 0;
        }

        public virtual Type GetRealType()
        {
            return null;
            //return NHibernateUtil.GetClass(this);
        }
    }
}
