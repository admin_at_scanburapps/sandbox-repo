﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScanburSandbox.Models
{
    public class Measurement : Entity
    {
        /// <summary>
        /// The unit for which this measurement has been measured.
        /// </summary>
        public ScanClimeUnit Unit { get; set; }
        [Column(Order =10)]
        public Guid? UnitId { get; set; }

        /// <summary>
        /// The datetime at which the measurement was made in the official SDM time. 
        /// </summary>
        [Column(Order = 11)]
        public DateTime? MeasurementTime { get; set; }

        public double? AirChangeActual { get; set;  }
        public double? RelativeHumidityRoomFixed { get; set; }
        public double? AirFlowDiff { get; set; }
        public double? AirvolumeDiff { get; set; }
        public double? ChillWaterValvePCT { get; set; }
        public double? HeaterTempSP { get; set; }
        public double? MixBedFilter { get; set; }
        public double? PressureDiff { get; set; }
        [MaxLength(20)]
        public String Timestamp { get; set; }
        public double? SteppermotorPCT { get; set; }
        public double? CageHumidity { get; set; }
        public double? InletTemperature { get; set; }



        internal static Measurement Create()
        {
            var m = new Measurement();
            m.Initialize();
            return m;
        }
    }
}
