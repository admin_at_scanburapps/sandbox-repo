﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ScanburSandbox.Models
{
    /// <summary>
    /// A convenience class used to implement some common stuff for all Model classes.
    /// We could have implemented this using components, but it just seemed more convenient like this.
    /// 
    /// </summary>
    public class Entity
    {
        [Key]
        [Column(Order = 1)]
        public Guid Id
        {
            get;
            protected set;
        }

        [NotMapped]
        public String IdStr
        {
            get
            {
                return Id.ToString();
            }
        }

        /// <summary>
        /// The timestamp for which this object was created the first time. 
        /// </summary>
        [Column(Order=2,TypeName ="datetime")]
        public DateTime CreatedAt
        {
            get; set;
        }



        /// <summary>
        /// The timestamp for which this object was most recently updated. 
        /// </summary>
        [Column(Order = 3, TypeName = "datetime")]
        public DateTime UpdatedAt
        {
            get; set;
        }

        public void OnSave()
        {
            this.UpdatedAt = DateTime.Now;
        }

        /// <summary>
        /// Call this when you want to initialize a newly created object. 
        /// </summary>
        public virtual void Initialize()
        {
            this.Id = Guid.NewGuid();
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }


        public override bool Equals(object obj)
        {
            var compareTo = obj as Entity;

            if (ReferenceEquals(compareTo, null))
                return false;

            if (ReferenceEquals(this, compareTo))
                return true;

            if (GetRealType() != compareTo.GetRealType())
                return false;

            if (!IsTransient() && !compareTo.IsTransient() && Id == compareTo.Id)
                return true;

            return false;
        }

        public virtual bool IsTransient()
        {
            return Id == null;
        }


        public static bool operator ==(Entity a, Entity b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(Entity a, Entity b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return (GetRealType().ToString() + Id).GetHashCode();
        }

        public virtual Type GetRealType()
        {
            return this.GetType();
        }
    }
}
